package ssm.controller;

import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.DEFAULT_IMAGE_CAPTION;
import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.model.SlideShowModel;
import ssm.view.SlideShowMakerView;
import ssm.model.Slide;

/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & ___Jake Burns__________
 */
public class SlideShowEditController {
    // APP UI
    private SlideShowMakerView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public SlideShowEditController(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES);
    }
    
    //moves selected slide up one
    public void processMoveUp(){
        SlideShowModel slideShow = ui.getSlideShow();
        Slide temp = slideShow.getSelectedSlide();
        int pos = slideShow.getSlides().lastIndexOf(temp);
        if (pos > 0)
        {
            slideShow.getSlides().set(pos, slideShow.getSlides().get(pos - 1));
            slideShow.getSlides().set(pos - 1, temp);
        }
        ui.reloadSlideShowPane(slideShow);
    }
    
    //moves selected slide down one spot
    public void processMoveDown(){
        SlideShowModel slideShow = ui.getSlideShow();
        Slide temp = slideShow.getSelectedSlide();
        int pos = slideShow.getSlides().indexOf(temp);
        if (pos < slideShow.getSlides().size() - 1)
        {
            slideShow.getSlides().set(pos, slideShow.getSlides().get(pos + 1));
            slideShow.getSlides().set(pos + 1, temp);
        }
        ui.reloadSlideShowPane(slideShow);
    }
    
    //removes selected slide
    public void processRemove(){
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.getSlides().remove(slideShow.getSelectedSlide());
        ui.reloadSlideShowPane(slideShow);
    }
}
