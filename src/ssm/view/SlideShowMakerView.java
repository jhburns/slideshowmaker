package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT;
import static ssm.LanguagePropertyType.TOOLTIP_LOAD_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_SAVE_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_MOVE_UP;
import static ssm.StartupConstants.ICON_MOVE_DOWN;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.StartupConstants.ICON_WINDOW;
import ssm.controller.FileController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 * @author McKilla Gorilla & _Jake Burns____________
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button exitButton;
    TextField titleInput;    
    
    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button upSlideButton;
    Button downSlideButton;
    Button removeSlideButton;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;
    
    SlideEditView selectedSlide;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	return slideShow;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
        
        //Change top icon to windows icon
        //Image icon = new Image(getClass().getResourceAsStream(PATH_ICONS + ICON_WINDOW));
        //primaryScene.getIcons().add(icon);
        
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	addSlideButton = this.initChildButton(slideEditToolbar,		ICON_ADD_SLIDE,	    TOOLTIP_ADD_SLIDE,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	upSlideButton = this.initChildButton(slideEditToolbar,		ICON_MOVE_UP,	    TOOLTIP_MOVE_UP,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        downSlideButton = this.initChildButton(slideEditToolbar,		ICON_MOVE_DOWN,	    TOOLTIP_MOVE_DOWN,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        removeSlideButton = this.initChildButton(slideEditToolbar,      ICON_REMOVE_SLIDE, TOOLTIP_REMOVE_SLIDE,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
         
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
        //slidesEditorScrollPane.getStyleClass().add("-fx-border-color: blue;");
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
    }

    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newSlideShowButton.setOnAction(e -> {
	    fileController.handleNewSlideShowRequest();
            titleInput.setVisible(true);
	});
	loadSlideShowButton.setOnAction(e -> {
	    fileController.handleLoadSlideShowRequest();
	});
	saveSlideShowButton.setOnAction(e -> {
	    fileController.handleSaveSlideShowRequest();
	});
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
	    editController.processAddSlideRequest();
            upSlideButton.setDisable(false);
            downSlideButton.setDisable(false);
            removeSlideButton.setDisable(false);
        });
        
        upSlideButton.setOnAction(e -> {
            editController.processMoveUp();
        });
        
        downSlideButton.setOnAction(e -> {
            editController.processMoveDown();
        });
        
        removeSlideButton.setOnAction(e -> {
            editController.processRemove();
        });
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW,	TOOLTIP_NEW_SLIDE_SHOW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW,	TOOLTIP_LOAD_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW,	TOOLTIP_SAVE_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW,	TOOLTIP_VIEW_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        //open slideshow when viewSlideShowButton is clicked
        viewSlideShowButton.setOnAction(e -> {
            SlideShowViewer viewer = new SlideShowViewer();
            Stage secondaryStage = new Stage();
            viewer.start(secondaryStage);
            
        });
	
        
        //create text field to enter title
        titleInput = new TextField();
        titleInput.setVisible(false);
        titleInput.setText(slideShow.getTitle());
        titleInput.setOnKeyTyped(e -> {
            slideShow.setTitle(titleInput.getText());
        });
        fileToolbarPane.getChildren().add(titleInput);
        
    }

    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
	ssmPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
	viewSlideShowButton.setDisable(false);
	
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
    }

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
	slidesEditorPane.getChildren().clear();
	for (Slide slide : slideShowToLoad.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(slide);
          
            //highlight slide when clicked
            slideEditor.setOnMouseClicked(e -> {
                if(selectedSlide != null)
                    selectedSlide.setStyle(null);
               slideShowToLoad.setSelectedSlide(slide);
               slideEditor.setStyle("-fx-border-color: blue;\n"
                + "-fx-border-width: 3;\n");
               selectedSlide = slideEditor;
            });
            
            if(slide == slideShowToLoad.getSelectedSlide())
                slideEditor.setStyle("-fx-border-color: blue;\n"
                + "-fx-border-width: 3;\n");
	    slidesEditorPane.getChildren().add(slideEditor);
            
	}
    }
    
    
    //Class to view SlideShow
    class SlideShowViewer extends Application
    {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Slide currentSlide;
        ImageView currentImg;
        Label caption;
        Label title;
        Button next;
        Button previous;
        BorderPane layout;
        int index = 0;
        
        @Override
        public void start(Stage viewer)
        {
            viewer.setTitle(props.getProperty(TITLE_WINDOW));
            
            currentImg = new ImageView();
            caption = new Label();
            title = new Label();
            
            next = new Button();
            previous = new Button();
            
            previous.setDisable(true);
            if(index >= slideShow.getSlides().size()- 1)
                    next.setDisable(true);
            
            next.setOnAction(e -> {
                if(index < slideShow.getSlides().size()- 1)
                    ++index;
                if(!next.isDisabled() && index >= slideShow.getSlides().size()- 1)
                    next.setDisable(true);
                if(previous.isDisabled() && index > 0)
                    previous.setDisable(false);
                
                reload(viewer);
            });
            
            previous.setOnAction(e -> {
                if(index > 0)
                    --index;
                if(!previous.isDisabled() && index == 0)
                    previous.setDisable(true);
                if(next.isDisabled() && index < slideShow.getSlides().size()- 1)
                    next.setDisable(false);
                
                reload(viewer);
            });
            
            String imagePath = "file:" + PATH_ICONS + ICON_NEXT;
            Image buttonNext = new Image(imagePath);
            next.setGraphic(new ImageView(buttonNext));
            
            imagePath = "file:" + PATH_ICONS + ICON_PREVIOUS;
            Image buttonPrev = new Image(imagePath);
            previous.setGraphic(new ImageView(buttonPrev));
            
            next.setTooltip(new Tooltip(props.getProperty(TOOLTIP_NEXT_SLIDE).toString()));
            previous.setTooltip(new Tooltip(props.getProperty(TOOLTIP_PREVIOUS_SLIDE).toString()));
            
            reload(viewer);
        }
        
        //reloads screen when called
        public void reload(Stage viewer)
        {
            currentSlide = slideShow.getSlides().get(index);
            
            
            String imagePath;
            imagePath = currentSlide.getImagePath() + SLASH + currentSlide.getImageFileName();
            File file = new File(imagePath);
            try {
                // GET AND SET THE IMAGE
                URL fileURL = file.toURI().toURL();
                Image slideImage = new Image(fileURL.toExternalForm());
                currentImg.setImage(slideImage);
                double scaledWidth = 1000;
                double perc = scaledWidth / slideImage.getWidth();
                double scaledHeight = slideImage.getHeight() * perc;
                currentImg.setFitWidth(scaledWidth);
                currentImg.setFitHeight(scaledHeight);
                } catch (Exception e) {
                    //display error dialog
                    ErrorHandler error = new ErrorHandler(new SlideShowMakerView(new SlideShowFileManager()));
                    error.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, e.getLocalizedMessage(), e.getMessage());
                }
            
            caption.setText(currentSlide.getCaption());
            title.setText(slideShow.getTitle());
            
            layout = new BorderPane();
            
            layout.setTop(title);
            layout.setCenter(currentImg);
            layout.setBottom(caption);
            layout.setRight(next);
            layout.setLeft(previous);
            
            // SET THE WINDOW TITLE
            //viewer.setTitle();
            
            // GET THE SIZE OF THE SCREEN
            Screen screen = Screen.getPrimary();
            Rectangle2D bounds = screen.getVisualBounds();

            // AND USE IT TO SIZE THE WINDOW
            viewer.setX(bounds.getMinX());
            viewer.setY(bounds.getMinY());
            viewer.setWidth(bounds.getWidth());
            viewer.setHeight(bounds.getHeight());

            Scene scene = new Scene(layout);
            
            viewer.setScene(scene);
            viewer.show();
        }
    }
}
